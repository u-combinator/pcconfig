;;; pacage --- Summary
;;; Commentary:
;;; Code:
(autoload 'haskell-mode-autoloads "Haskell Mode" "Major mode for edit Haskell code." t nil)
(autoload 'ghc "ghc" t nil)
(autoload 'ghc-init "ghc" nil t)
(autoload 'ghc-debug "ghc" nil t)

(eval-after-load "which-function-mode"
  '(add-to-list 'which-function-mode 'haskell-mode))

(provide 'mine-haskell)
;;; mine-haskell ends here