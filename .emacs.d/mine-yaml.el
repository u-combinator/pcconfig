;;; pacage --- Summary
;;; Commentary:
;;; Code:
(require 'yaml-mode)
(add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))
(provide 'mine-yaml)
;;; mine-yaml.el ends here