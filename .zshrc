autoload -U colors && colors
autoload -U compinit promptinit
compinit
promptinit
setopt correctall
prompt fire skyblue orange  magenta brown yellow white

bindkey -e

export HISTSIZE=10000
LS_COLORS=$LC_COLORS:'di=1;31'
export LS_COLORS

alias ls='ls --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias ll='ls -l --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias la='ls -a --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias lal='ls -la --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias grep='grep --color=tty -d skip'
alias cp="cp -i"                          # confirm before overwriting something
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias np='nano PKGBUILD'

alias bintime='/usr/bin/time -v'

alias suna='sudo nano'
alias srm='sudo rm'
alias scp='sudo cp -i'
alias scpr='sudo cp -Rf'
alias smv='sudo mv'
alias chm='chmod 755'
alias chmr='chmod 755 -R'
alias suchm='sudo chmod 755'
alias suchmr='sudo chmod 755 -R'
alias cho='chown ucombinator'
alias chor='chown ucombinator -R'
alias sucho='sudo chown ucombinator'
alias suchor='sudo chown ucombinator -R'
alias gztar='tar zvxf'
alias bztar='tar jvxf'
alias cdup='cd ..'
alias cd2up='cd ../..'
alias cd3up='cd ../../..'
alias frht='free -ht'
alias dfht='df -hT --total'
alias dush='du -sh'
alias mkpd='mkdir -p'
alias frht='free -ht'
alias difu='diff -u'
alias wats='whatis'
alias seps='ps aux | grep'
alias shutnow='shutdown -h now'
alias sushut='sudo shutdown -h'

alias memcpu='ps -eo pcpu,pmem,pid,user,args | sort -r -k1 | less'

alias emsbatch='emacs -batch -f batch-byte-compile'

alias cl11='clang -std=c11'
alias cl99='clang -std=c99'
alias cp14='clang++ -std=c++1y'
alias cp11='clang++ -std=c++11'
alias gc11='gcc -std=c11'
alias gc99='gcc -std=c99'
alias gp11='g++ -std=c++11'
alias gp14='g++ -std=c++1y'
alias g3compile='gcc `pkg-config --cflags gtk+-3.0 --libs gtk+-3.0` -o'
alias c3compile='clang `pkg-config --cflags gtk+-3.0 --libs gtk+-3.0`'

alias vleakshow='valgrind --leak-check=full --show-leak-kinds=all'

alias psync='sudo pacman -Sy'
alias pupdt='sudo pacman -Syu'
alias pkgins='sudo pacman -S'
alias plins='sudo pacman -U'
alias ssync='sudo pacman -Syy'
alias supdt='sudo pacman -Syyu'
alias pkgdl='sudo pacman -Rsn'
alias pkgin='pacman -Si'
alias pkgse='pacman -Ss'
alias pkglin='pacman -Qi'
alias pkglse='pacman -Qs'
alias pkglc='pacman -Ql'
alias pkgcl='sudo rm /var/cache/pacman/pkg/*'

alias sysstart='sudo systemctl start'
alias sysstop='sudo systemctl stop'
alias sysdis='sudo systemctl disable'
alias sysena='sudo systemctl enable'

alias kill9='kill -9'
alias kall='killall'
alias sukill='sudo kill'
alias sukall='sudo killall'
alias sukill9='sudo kill -9'

alias gad='git add'
alias gcom='git commit -m'
alias gcoma='git commit -a'
alias gpum='git push -u origin master'
alias gpom='git push origin master'
alias grm='git rm'
alias gmv='git mv'
alias grmv='git remote mv'
alias grad='git remote add origin'
